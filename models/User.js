const mongoose = require("mongoose")

const user_schema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	}, 
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	}, 
	mobileNo: String
})

module.exports = mongoose.model("User", user_schema)