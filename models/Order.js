const mongoose = require("mongoose")

const order_schema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "User ID is required"]
			},
			quantity: Number
		}
	],
	totalAmount: Number,
	purchasedOn: {
		type: Date,
		default: new Date()
	} 
})

module.exports = mongoose.model("Order", order_schema)