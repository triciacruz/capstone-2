const mongoose = require("mongoose")

const product_schema = new mongoose.Schema({
	name: String,
	description: String,
	price: Number,
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	} 
})

module.exports = mongoose.model("Product", product_schema)