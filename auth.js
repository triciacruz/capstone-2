const jwt = require("jsonwebtoken")

const secret = "ECommerceAPI" //secret key to be used for validating the token

module.exports.createAccessToken = (user) => {
	const user_data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(user_data, secret, {})
}
//end for first auth code

//CREATE USER CONTROLLER FIRST

//start second and last auth code
//Verify the validity of token (custom middleware)
module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {

			if(error){
				return response.send({auth: "Verification failed."})
			}

			next()
		})
	}else {

		return response.send({auth: "Token is not defined."})
	}
}


//To get user data from the token
module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return null
			}

			return jwt.decode(token, {complete: true}).payload
		})
	}else {
		return null
	}
}