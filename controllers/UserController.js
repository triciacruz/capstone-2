const User = require ("../models/User")
const Product = require ("../models/Product")
const Order = require ("../models/Order")
const auth = require ("../auth")
const bcrypt = require ("bcrypt")


//Check if email exists
module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then(result => {
		
		if(result.length > 0) {
			return {
				message: "User exist!"
			}
		}

		return {
			message: "User does not exist! Please register."
		}
	})
}


module.exports.registerUser = (request_body) => {
	let new_user = new User({
		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
		mobileNo: request_body.mobileNo,
		password: bcrypt.hashSync(request_body.password, 10)
	})

	return new_user.save().then((registered_user, error) => {
		if(error){
			return error 
		} 

		return {
			message: 'User successfully registered!'
		}
	})
}


module.exports.loginUser = (request_body) => {
	return User.findOne({email: request_body.email}).then(result => {
		
		if(result === null) {
			return {
				message: "The user doesn't exist." 
			}
		}

		const is_password_correct = bcrypt.compareSync(request_body.password, result.password)


		if(is_password_correct){
			return{
				accessToken: auth.createAccessToken(result.toObject())
			}
		}

		return {
			message: "The email and password combination is not correct!"
		}
	})
}
//end for user controller (first part) code. creating the route (first part).


//second part code start
//For getting user details from the token
module.exports.getProfile = (userId) => {
	return User.findById(userId).then(result => {
		return result
	})
}
//go to routes


//additional
//Get all users
module.exports.getAll = () => {
	return User.find({}).then((user, error) => {
		if (error){
			return error
		}
		return user
	})
}


//Get all admin
module.exports.getAllAdmin = () => {
	return User.find({isAdmin: true}).then(admin => {
		return admin
	})
}

//Get all non-admin
module.exports.getAllNonAdmin = () => {
	return User.find({isAdmin: false}).then(admin => {
		return admin
	})
}

//additional
//Set user as Admin
module.exports.setUserAsAdmin = (user_id, new_status) => {

	let setAsAdmin = {
		email: new_status.email,
		isAdmin: new_status.isAdmin
	}

	return User.findByIdAndUpdate(user_id, setAsAdmin).then((modified_userStatus, error) => {
		
		if(error){
			return error
		}

		return {
			message: "User has been set as admin.",
			data: modified_userStatus
		}
	})
}
