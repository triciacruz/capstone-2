const Product = require ("../models/Product")


//Get all products
module.exports.getAll = () => {
	return Product.find({}).then((product, error) => {
		if (error){
			return error
		}
		return product
	})
}


//Get all active products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(active_products => {
		return active_products
	})
}


//Get all not active products
module.exports.getAllNotActive = () => {
	return Product.find({isActive: false}).then(notActive_products => {
		return notActive_products
	})
}


//Get single product
module.exports.getProduct = (product_id) => {
	return Product.findById(product_id).then(result => {
		return result
	})
}


//Create new product
module.exports.createProduct = (request_body) => {
	
	let new_product = new Product({
		name: request_body.name,
		description: request_body.description,
		price:request_body.price
	})

	return new_product.save().then((created_product, error) => {
		
		if(error){
			return error
		}

		return {
			message: "Product created successfully!",
			data: created_product
		}
		
	})
}


//Update existing products
module.exports.updateProduct = (product_id, new_content) => {

	let updated_product = {
		name: new_content.name,
		description: new_content.description,
		price:new_content.price
	}

	return Product.findByIdAndUpdate(product_id, updated_product).then((modified_product, error) => {
		
		if(error){
			return error
		}

		return {
			message: "Product updated successfully!",
			data: modified_product
		}
	})
}


//Archive product
module.exports.archiveProduct = (product_id) => {
	return Product.findById(product_id).then((product, error) => {

	if(error){
			return error
		}

	product.isActive = false

		return product.save().then((archived_products, error) => {
			if(error){
				return error
			}

			return archived_products
		})
 	})
}


//Activate products
module.exports.activateProduct = (product_id) => {
	return Product.findById(product_id).then((product, error) => {

	if(error){
			return error
		}

	product.isActive = true

		return product.save().then((activated_products, error) => {
			if(error){
				return error
			}

			return activated_products
		})
 	})
}
