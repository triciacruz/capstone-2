const Order = require ("../models/Order")


// Create new order checkout
module.exports.createOrder = (request_body) => {
  const { userId, products, totalAmount } = request_body;

  if (!userId) {
    return Promise.reject("User ID is required");
  }

  const new_order = new Order({
    userId,
    products,
    totalAmount
  });

  return new_order
    .save()
    .then((created_order) => {
      return {
        message: "Order created successfully!",
        data: created_order
      };
    })
    .catch((error) => {
      throw error;
    });
};



//Get all orders
module.exports.getAll = () => {
	return Order.find({}).then((order, error) => {
		if (error){
			return error
		}
		return order
	})
}


//Get single order 
module.exports.getOrder = (order_id) => {
	return Order.findById(order_id).then(result => {
		return result
	})
}

//get order by product
module.exports.getOrdersByProduct = (product_id) => {
	return Order.findById(product_id).then(result => {
		return result
	})
}