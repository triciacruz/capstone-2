const express = require("express")
const router = express.Router()
const auth = require("../auth")
const UserController = require("../controllers/UserController")

	
//Check email for existing user
router.post("/check-email", (request, response) => {
	UserController.checkEmailExists(request.body).then(result => response.send(result))
})


//Register User
router.post("/register", (request, response) => {
	UserController.registerUser(request.body).then(result => response.send(result))
})


//Login user
router.post("/login", (request, response) => {
	UserController.loginUser(request.body).then(result => response.send(result))
})
//end for userRoutes (first part)
//postman first part


//second part start (from UserController)
//Get users details from token
router.get("/details", auth.verify, (request, response) => {

	const user_data = auth.decode(request.headers.authorization)

	UserController.getProfile(user_data.id).then(result => response.send(result))
})


//additional
//get all users
router.get("/",  auth.verify, (request, response) => {
	UserController.getAll().then(result => response.send(result))
})


//Get all admin
router.get("/admin", auth.verify, (request, response) => {
	UserController.getAllAdmin().then(result => response.send(result))
})


//Get all non-admin
router.get("/non-admin", auth.verify, (request, response) => {
	UserController.getAllNonAdmin().then(result => response.send(result))
})


//additional
//set user as admin
router.patch("/:id/setAdmin", auth.verify, (request, response) => {
	UserController.setUserAsAdmin(request.params.id, request.body).then(result => response.send(result))
})

module.exports = router