const express = require("express")
const router = express.Router()
const ProductController = require("../controllers/ProductController")
const auth = require("../auth")


//Get all products
router.get("/all", (request, response) => {
	ProductController.getAll().then(result => response.send(result))
})


//Get all active products
router.get("/active", (request, response) => {
	ProductController.getAllActive().then(result => response.send(result))
})


//Get all not active products
router.get("/not-active", (request, response) => {
	ProductController.getAllNotActive().then(result => response.send(result))
})


//Get single product
router.get("/:id", (request, response) => {
	ProductController.getProduct(request.params.id).then(result => response.send(result))
})


//Create new product
router.post("/add", auth.verify, (request, response) => {
	ProductController.createProduct(request.body).then(result => response.send(result))
})


//Update details of existing product
router.patch("/:id/update", auth.verify, (request, response) => {
	ProductController.updateProduct(request.params.id, request.body).then(result => response.send(result))
})


//Archived products
router.patch("/:id/archive", auth.verify, (request, response) => {
	ProductController.archiveProduct(request.params.id, request.body).then(result => response.send(result))
})


//Activate products
router.patch("/:id/activate", auth.verify, (request, response) => {
	ProductController.activateProduct(request.params.id, request.body).then(result => response.send(result))
})


module.exports = router