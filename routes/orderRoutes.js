const express = require("express")
const router = express.Router()
const OrderController = require("../controllers/OrderController")
const auth = require("../auth")
const Order = require('../models/Order');

//Create new order checkout
/*
router.post("/checkout", (request, response) => {
	OrderController.createOrder(request.body).then(result => response.send(result))
})
*/

/*router.post('/checkout', (request, response) => {
  const { userId, productId } = request.body;

  const result = {
    message: 'Order created successfully',
    userId,
    productId,
  };

  response.send(result);
});
*/
router.post('/checkout', (request, response) => {
  const { userId, products, totalAmount } = request.body;

  const newOrder = new Order({
    userId: userId,
    products: products,
    totalAmount: totalAmount,
  });

  newOrder
    .save()
    .then((createdOrder) => {
      const result = {
        message: 'Order created successfully',
        orderId: createdOrder._id,
      };

      response.send(result);
    })
    .catch((error) => {
      response.status(500).json({ error: error.message });
    });
});


//Get all orders
router.get("/", auth.verify, (request, response) => {
	OrderController.getAll().then(result => response.send(result))
})


//Get single order
router.get("/:id", auth.verify, (request, response) => {
	OrderController.getOrder(request.params.id).then(result => response.send(result))
})

//get orders by product
router.get("/:id/by-product", auth.verify, (request, response) => {
	OrderController.getOrdersByProduct(request.params.id).then(result => response.send(result))
})


module.exports = router